terraform {
  required_version = "~> 1.3.0"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "~> 0.7.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.4.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 3.2.0"
    }
    powerdns = {
      source  = "pan-net/powerdns"
      version = "~> 1.5.0"
    }
    rancher2 = {
      source  = "rancher/rancher2"
      version = "~> 1.25.0"
    }
  }
}

# Configure the libvirt provider
provider "libvirt" {
  uri = var.libvirt_uri
}

# Configure the PowerDNS provider
provider "powerdns" {
  api_key    = var.pdns_api_key
  server_url = var.pdns_server_url
}

# Configure the Rancher2 provider to admin
provider "rancher2" {
  api_url   = var.rancher2_api_url
  token_key = var.rancher2_token_key
}

# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "ubuntu_qcow2" {
  name   = "${var.prefix}-ubuntu.qcow2"
  pool   = var.libvirt_volume_pool
  source = var.libvirt_volume_source
  format = "qcow2"
}

module "cluster" {
  source = "git::https://gitlab.com/2stacks-pub/terraform-libvirt-ubuntu.git//.?ref=v1.2.2"

  count = var.guest_count

  hostname            = format("${var.prefix}-%02d", count.index + 1)
  vcpu                = "6"
  memory              = "12288"
  libvirt_volume_pool = var.libvirt_volume_pool
  libvirt_volume_size = 64424509440
  libvirt_volume_id   = libvirt_volume.ubuntu_qcow2.id
  network             = "ovs-network"
  port_group          = "v6-pub"

  # Cloud-Init using No-Cloud, see - https://cloudinit.readthedocs.io/en/latest/topics/datasources/nocloud.html#datasource-nocloud
  user_data = templatefile("${path.module}/templates/cloud_init.yml.tftpl", {
    user_name          = var.user_name
    user_passwd        = var.user_passwd
    ssh_authorized_key = var.ssh_authorized_key
    package_list       = var.package_list
  })
  meta_data = templatefile("${path.module}/templates/meta_data.yml.tftpl", {
    hostname = format("${var.prefix}-%02d", count.index + 1)
  })
  network_config = file("${path.module}/templates/network_config.yml")
}


# Add multiple A records for rke2 endpoint
resource "powerdns_record" "rke2_vip" {
  zone    = "${var.hosted_domain}."
  name    = "${var.name}.${var.hosted_domain}."
  type    = "A"
  ttl     = 300
  records = module.cluster[*].ipv4_address
}

# Add A record for each instance
resource "powerdns_record" "rke2_instance" {
  count = var.guest_count

  zone    = "${var.hosted_domain}."
  name    = "${module.cluster[count.index].instance_name}.${var.hosted_domain}."
  type    = "A"
  ttl     = 300
  records = [module.cluster[count.index].ipv4_address]
}

# Create a new rancher v2 RKE2 custom Cluster v2
resource "rancher2_cluster_v2" "rke2" {
  name                                     = var.name
  kubernetes_version                       = "v1.23.8+rke2r1"
  enable_network_policy                    = false
  default_cluster_role_for_project_members = "user"
}

resource "local_sensitive_file" "kube_config" {
  count = var.write_config ? 1 : 0

  content  = rancher2_cluster_v2.rke2.kube_config
  filename = "${path.module}/config.yaml"
}

# Join nodes to cluster
resource "null_resource" "bootstrap" {
  count = var.guest_count

  connection {
    host    = module.cluster[count.index].ipv4_address
    type    = "ssh"
    agent   = true
    user    = var.user_name
    timeout = "5m"
  }

  provisioner "remote-exec" {
    inline = [
      "${rancher2_cluster_v2.rke2.cluster_registration_token[0].node_command} --etcd --controlplane --worker",
    ]
  }

  depends_on = [powerdns_record.rke2_vip, rancher2_cluster_v2.rke2]
}
