# terraform-rancher-libvirt-rke2

Deploys an RKE2 cluster using Terraform, [terraform-libvirt-ubuntu](https://gitlab.com/2stacks/terraform-libvirt-ubuntu) and the [rancher2](https://registry.terraform.io/providers/rancher/rancher2/latest/docs/resources/cluster_v2) Terraform provider.

## Table of Contents
- [Usage](#usage)
- [Requirements](#requirements)
- [Providers](#providers)
- [Modules](#modules)
- [Resources](#resources)
- [Inputs](#inputs)
- [Outputs](#outputs)
- [Contributing](#contributing)

## Usage
```bash
terraform init
terraform plan
terraform apply
```

### Pre-requisites
<!-- Describe external dependencies or pre-requisites -->
- [kvm](https://www.linux-kvm.org/page/Main_Page)
- [libvirt](https://libvirt.org/)
- [xsltproc](http://xmlsoft.org/xslt/xsltproc2.html)

#### NOTE
The host responsible for running this module must have `xsltproc` installed.  The following error indicates this package has not been installed.  Consult your OSs package manager or the link above for instructions on installing this dependency.
```bash
│ Error: error applying XSLT stylesheet: exec: "xsltproc": executable file not found in $PATH
│
│   with libvirt_domain.ubuntu[0],
│   on main.tf line 77, in resource "libvirt_domain" "ubuntu":
│   77: resource "libvirt_domain" "ubuntu" {
```

### Quick Start
```bash
terraform init
terraform plan
terraform apply
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.3.0 |
| <a name="requirement_libvirt"></a> [libvirt](#requirement\_libvirt) | ~> 0.7.0 |
| <a name="requirement_local"></a> [local](#requirement\_local) | ~> 2.4.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | ~> 3.2.0 |
| <a name="requirement_powerdns"></a> [powerdns](#requirement\_powerdns) | ~> 1.5.0 |
| <a name="requirement_rancher2"></a> [rancher2](#requirement\_rancher2) | ~> 1.25.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_libvirt"></a> [libvirt](#provider\_libvirt) | 0.7.1 |
| <a name="provider_local"></a> [local](#provider\_local) | 2.4.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.2.1 |
| <a name="provider_powerdns"></a> [powerdns](#provider\_powerdns) | 1.5.0 |
| <a name="provider_rancher2"></a> [rancher2](#provider\_rancher2) | 1.25.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_cluster"></a> [cluster](#module\_cluster) | git::https://gitlab.com/2stacks-pub/terraform-libvirt-ubuntu.git//. | v1.2.2 |

## Resources

| Name | Type |
|------|------|
| [libvirt_volume.ubuntu_qcow2](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/volume) | resource |
| [local_sensitive_file.kube_config](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/sensitive_file) | resource |
| [null_resource.bootstrap](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [powerdns_record.rke2_instance](https://registry.terraform.io/providers/pan-net/powerdns/latest/docs/resources/record) | resource |
| [powerdns_record.rke2_vip](https://registry.terraform.io/providers/pan-net/powerdns/latest/docs/resources/record) | resource |
| [rancher2_cluster_v2.rke2](https://registry.terraform.io/providers/rancher/rancher2/latest/docs/resources/cluster_v2) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_guest_count"></a> [guest\_count](#input\_guest\_count) | Number of Guests to Create | `number` | `3` | no |
| <a name="input_hosted_domain"></a> [hosted\_domain](#input\_hosted\_domain) | PowerDNS Zone Name | `string` | n/a | yes |
| <a name="input_libvirt_uri"></a> [libvirt\_uri](#input\_libvirt\_uri) | URI of server running libvirtd | `string` | `"qemu:///system"` | no |
| <a name="input_libvirt_volume_pool"></a> [libvirt\_volume\_pool](#input\_libvirt\_volume\_pool) | Volume Storage Pool | `string` | `"default"` | no |
| <a name="input_libvirt_volume_source"></a> [libvirt\_volume\_source](#input\_libvirt\_volume\_source) | Volume Image Source | `string` | `"https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-disk-kvm.img"` | no |
| <a name="input_name"></a> [name](#input\_name) | Cluster Name | `string` | `"apps"` | no |
| <a name="input_package_list"></a> [package\_list](#input\_package\_list) | List of additional packages to install | `list(string)` | <pre>[<br>  "qemu-guest-agent"<br>]</pre> | no |
| <a name="input_pdns_api_key"></a> [pdns\_api\_key](#input\_pdns\_api\_key) | The PowerDNS API key. This can also be specified with PDNS\_API\_KEY environment variable | `string` | n/a | yes |
| <a name="input_pdns_server_url"></a> [pdns\_server\_url](#input\_pdns\_server\_url) | The address of PowerDNS server. This can also be specified with PDNS\_SERVER\_URL environment variable | `string` | n/a | yes |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Resources will be prefixed with this to avoid clashing names | `string` | `"apps"` | no |
| <a name="input_rancher2_api_url"></a> [rancher2\_api\_url](#input\_rancher2\_api\_url) | Rancher API url. It must be provided, but it can also be sourced from the RANCHER\_URL environment variable | `string` | n/a | yes |
| <a name="input_rancher2_token_key"></a> [rancher2\_token\_key](#input\_rancher2\_token\_key) | Rancher API token key to connect to rancher. It can also be sourced from the RANCHER\_TOKEN\_KEY environment variable | `string` | n/a | yes |
| <a name="input_ssh_authorized_key"></a> [ssh\_authorized\_key](#input\_ssh\_authorized\_key) | SSH Public Key for user | `string` | n/a | yes |
| <a name="input_user_name"></a> [user\_name](#input\_user\_name) | Host ssh user name | `string` | `"ubuntu"` | no |
| <a name="input_user_passwd"></a> [user\_passwd](#input\_user\_passwd) | SSH user console password | `string` | n/a | yes |
| <a name="input_write_config"></a> [write\_config](#input\_write\_config) | Write Kube Config generated for the cluster v2 to file config.yaml | `bool` | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_module_output"></a> [module\_output](#output\_module\_output) | Return all module outputs |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Contributing
Code quality and security will be validated before merge requests are accepted.

### Tools
These tools are used to ensure validation and standardization of Terraform deployments

#### Must be installed
- [pre-commit](https://github.com/gruntwork-io/pre-commit/releases)
- [terraform-docs](https://github.com/terraform-docs/terraform-docs)
- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/aquasecurity/tfsec)

#### Provided by Terraform
- [terraform fmt](https://www.terraform.io/docs/commands/fmt.html)
- [terraform validate](https://www.terraform.io/docs/commands/validate.html)

For more information see - [pre-commit-hooks-for-terraform](https://medium.com/slalom-build/pre-commit-hooks-for-terraform-9356ee6db882)

### To submit a merge request
```bash
git checkout -b <branch name>
pre-commit autoupdate
pre-commit run -a
git commit -a -m 'Add new feature'
git push origin <branch name>
```
Optionally run the following to automate the execution of pre-commit on every git commit.
```bash
pre-commit install
```

# License
Copyright (c) 2022 [2stacks.net](www.2stacks.net)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
