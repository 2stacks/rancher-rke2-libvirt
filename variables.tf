# Libvirt Variables
variable "libvirt_uri" {
  description = "URI of server running libvirtd"
  type        = string
  default     = "qemu:///system"
}

# Libvirt Instance Settings
variable "guest_count" {
  description = "Number of Guests to Create"
  type        = number
  default     = 3
}

variable "prefix" {
  description = "Resources will be prefixed with this to avoid clashing names"
  type        = string
  default     = "apps"
}

variable "libvirt_volume_source" {
  description = "Volume Image Source"
  type        = string
  default     = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-disk-kvm.img"
}

variable "libvirt_volume_pool" {
  description = "Volume Storage Pool"
  type        = string
  default     = "default"
}

# CloudInit Variables
variable "user_name" {
  description = "Host ssh user name"
  type        = string
  default     = "ubuntu"
}

variable "user_passwd" {
  description = "SSH user console password"
  type        = string
}

variable "ssh_authorized_key" {
  description = "SSH Public Key for user"
  type        = string
}

variable "package_list" {
  description = "List of additional packages to install"
  type        = list(string)
  default = [
    "qemu-guest-agent",
  ]
}

# PowerDNS Variables
variable "pdns_api_key" {
  description = "The PowerDNS API key. This can also be specified with PDNS_API_KEY environment variable"
  type        = string
}

variable "pdns_server_url" {
  description = "The address of PowerDNS server. This can also be specified with PDNS_SERVER_URL environment variable"
  type        = string
}

variable "hosted_domain" {
  description = "PowerDNS Zone Name"
  type        = string
}

variable "name" {
  description = "Cluster Name"
  type        = string
  default     = "apps"
}

# Rancher Variables
variable "rancher2_api_url" {
  description = "Rancher API url. It must be provided, but it can also be sourced from the RANCHER_URL environment variable"
  type        = string
}

variable "rancher2_token_key" {
  description = "Rancher API token key to connect to rancher. It can also be sourced from the RANCHER_TOKEN_KEY environment variable"
  type        = string
}

variable "write_config" {
  description = "Write Kube Config generated for the cluster v2 to file config.yaml"
  type        = bool
  default     = false
}
